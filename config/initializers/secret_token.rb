# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'b3c648ea7e456157422cf775d216138ab2ba127dae1654c3e5fdf0f92be59acc3eb1b41286d3b138e95c018b218045b4711094baf77ddaa1764cb025092a7661'
